import { Observable } from "../observable.js";
import { Subscription } from "../subscription.js";
import { finalize } from "./pipes.js";

export function of(...src) {
  return new Observable(observer => {
    src.forEach(e => {
      observer.next(e);
    });
    observer.complete();
  });
}

export function from(src) {
  return new Observable(observer => {
    let closed = false;

    if (src instanceof Observable) {
      const subscription = src.subscribe({
        next: e => observer.next(e),
        error: e => observer.error(e),
        complete: () => observer.complete()
      });
      return () => subscription.unsubscribe();
    }
    if (Array.isArray(src) && !closed) {
      for (let key in src) {
        observer.next(src[key]);
      }
      observer.complete();
    }
    if (src instanceof Promise) {
      src.then(value => {
        if (!closed) {
          observer.next(value);
          observer.complete();
        }
      }).catch(err => {
        if (!closed) observer.error(err)
      });
    }
    return () => closed = true;
  });
}

export function interval(interv) {
  return new Observable(observer => {
    let i = 0;

    const id = setInterval(() => {
      observer.next(i++);
    }, interv);

    return () => clearInterval(id);
  });
}

export function fromEventTarget(emitter, events) {
  if (!Array.isArray(events)) {
    events = [events];
  }
  return new Observable(observer => {
    const handler = event => observer.next(event);

    events.forEach(eventName => {
      emitter.addEventListener(eventName, handler);
    });
    return () => {
      events.forEach(eventName => {
        emitter.removeEventListener(eventName, handler)
      });
    };
  });
}

export function fromEventSource(url, events) {  
  const source = new EventSource(url);
  return fromEventTarget(source, events)
    .pipe(finalize(() => source.close()));
}

export function merge(...sources) {
  return new Observable(observer => {
    const subscription = new Subscription();
    sources.forEach(source => {
      subscription.add(
        source.subscribe({
          next: value => observer.next(value),
          error: err => observer.error(err),
          complete: () => observer.complete()
        })
      );
    });
    return () => subscription.unsubscribe();
  });
}

import { Subscription } from "../subscription.js";
import { Observable } from "../observable.js";
import { from } from "./sources.js";

export function map(transform) {
  return subscriber => {
    return new Observable(observer => {
      let index = 0;
      const subscription = subscriber.subscribe({
        next: value => observer.next(transform(value, index++)),
        error: err => observer.error(err),
        complete: () => observer.complete()
      });
      return () => subscription.unsubscribe();
    });
  };
}

export function filter(condition) {
  return subscriber => {
    return new Observable(observer => {
      let index = 0;
      const subscription = subscriber.subscribe({
        next: value => {
          if (condition(value, index++)) {
            observer.next(value);
          }
        },
        error: err => observer.error(err),
        complete: () => observer.complete()
      });
      return () => subscription.unsubscribe();
    });
  };
}

export function first() {
  return subscriber => {
    return new Observable(observer => {
      const subscription = subscriber.subscribe({
        next: value => {
          observer.next(value);
          observer.complete();
        },
        error: err => observer.error(err),
        complete: () => observer.complete()
      });
      return () => subscription.unsubscribe();
    });
  };
}

export function last() {
  return subscriber => {
    return new Observable(observer => {
      let lastValue = void 0;
      const subscription = subscriber.subscribe({
        next: value => {
          lastValue = value;
        },
        error: err => observer.error(err),
        complete: () => {
          observer.next(lastValue);
          observer.complete();
        }
      });
      return () => subscription.unsubscribe();
    });
  };
}

export function take(limit) {
  return subscriber => {
    return new Observable(observer => {
      let count = 0;
      const subscription = subscriber.subscribe({
        next: value => {
          if (count++ < limit) {
            observer.next(value);
          }
          if (count >= limit) {
            observer.complete()
          }
        },
        error: err => observer.error(err),
        complete: () => observer.complete()
      });
      return () => subscription.unsubscribe();
    });
  };
}

export function timeout(time) {
  return subscriber => {
    return new Observable(observer => {
      const _timeout = setTimeout(() => observer.error(new Error("Timeout error")), time);
      const subscription = subscriber.subscribe({
        next: value => observer.next(value),
        error: err => observer.error(err),
        complete: () => observer.complete()
      });
      return () => {
        clearTimeout(_timeout);
        subscription.unsubscribe();
      };
    });
  };
}

export function keepAlive(time) {
  return subscriber => {
    return new Observable(observer => {
      const timeout = setTimeout(() => observer.complete(), time);
      const subscription = subscriber.subscribe({
        next: value => observer.next(value),
        error: err => observer.error(err),
        complete: () => observer.complete()
      });
      return () => {
        clearTimeout(timeout);
        subscription.unsubscribe();
      };
    });
  };
}

export function retry(optionsOrCount) {
  const options = {
    count: Infinity,
    delay: 0,
    resetOnSuccess: false
  };

  if (typeof optionsOrCount === "object") {
    options.count = optionsOrCount.count === void 0 ? options.count : optionsOrCount.count;
    options.delay = optionsOrCount.delay === void 0 ? options.delay : optionsOrCount.delay;
    options.resetOnSuccess = optionsOrCount.resetOnSuccess || options.resetOnSuccess;
  } else if (typeof optionsOrCount === "number") {
    options.count = optionsOrCount;
  }
  return subscriber => {
    return new Observable(observer => {
      let subscription = void 0;
      let count = 0;
      let timeout = void 0;

      const resubscribe = observe => {
        if (timeout) {
          clearTimeout(timeout);
          timeout = void 0;
        }
        timeout = setTimeout(() => {
          if (subscription) subscription.unsubscribe();
          subscription = subscriber.subscribe(observe);
        }, options.delay);
      };

      const observe = {
        next: value => {
          if (options.resetOnSuccess) {
            count = 0;
          }
          observer.next(value);
        },
        error: err => {
          if (count++ < options.count) {
            resubscribe(observe);
          } else {
            observer.error(err);
          }
        },
        complete: () => observer.complete()
      };
      subscription = subscriber.subscribe(observe);
      return () => {
        clearTimeout(timeout);
        subscription.unsubscribe();
      };
    });
  };
}

export function finalize(callback) {
  return subscriber => {
    return new Observable(observe => {
      const subscription = subscriber.subscribe({
        next: value => observe.next(value),
        error: err => observe.error(err),
        complete: () => observe.complete()
      });
      return () => {
        subscription.unsubscribe();
        callback ? callback() : void 0;
      }
    });
  }
}

export function catchError(callback) {
  return subscriber => {
    return new Observable(observe => {
      const subscription = new Subscription();

      subscription.add(
        subscriber.subscribe({
          next: value => observe.next(value),
          error: err => {
            const ret = from(callback(err, subscriber));
            subscription.add(
              ret.subscribe({
                next: e => observe.next(e),
                error: e => observe.error(e),
                complete: () => observe.complete()
              })
            );
          },
          complete: () => observe.complete()
        })
      );
      return () => subscription.unsubscribe();
    });
  }
}

export function throwIf(condition, error) {
  return subject => new Observable(observer => {
    const subscription = subject.subscribe({
      next: e => condition(e) ? observer.error(error || e) : observer.next(e),
      error: e => observer.error(e),
      complete: () => observer.complete()
    });
    return () => subscription.unsubscribe()
  });
}

export function switchMap(transform) {
  return subscriber => {
    return new Observable(observer => {
      let index = 0;
      let isComplete = false;
      let countInner = 0;

      const checkComplete = () => isComplete && !countInner && observer.complete();

      const subscription = subscriber.subscribe({
        next: value => {
          countInner++;
          from(transform(value, index++))
            .subscribe({
              next: e => observer.next(e),
              error: e => observer.error(e),
              complete: () => {
                countInner--;
                checkComplete();
              }
            });
        },
        error: err => observer.error(err),
        complete: () => {
          isComplete = true;
          checkComplete();
        }
      });
      return () => subscription.unsubscribe();
    });
  };
}

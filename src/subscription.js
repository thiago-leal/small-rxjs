export class Subscription {
	#closed = false;
	#subscriptions = [];

	add(subscription) {
		if (typeof subscription.unsubscribe === "function") {
			if (this.#closed) subscription.unsubscribe();
			else this.#subscriptions.push(subscription);
		}
	}

	insertIn(subscription) {
		if (subscription instanceof Subscription) {
			subscription.add(this);
		}
	}

	unsubscribe() {
		if (!this.#closed) {
			this.#closed = true;
			this.#subscriptions.forEach(subscription => subscription.unsubscribe());
		}
	}
}

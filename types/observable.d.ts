import { Observer } from "./observer"
import { Subscription } from "./subscription"

export type ValueOrPromise<T> = T | Promise<T>;

export interface UnaryFunction<T, R> {
  (source: T): R
}

export class Observable<T> {
	constructor(func: (subject: Observer<T>) => ValueOrPromise<void | (() => void)>);

	subscribe(observer: ((value: T) => any) | {
    next?: (value: T) => any
    error?: (value: any) => any
    complete?: () => any
  }): Subscription;

	toPromise(): Promise<T>;

  pipe(): Observable<T>;
  pipe<A>(transform: UnaryFunction<T, A>): Observable<A>;
  pipe<A, B>(
    transform0: UnaryFunction<T, A>,
    transform1: UnaryFunction<A, B>
  ): Observable<B>;
  pipe<A, B, C>(
    transform0: UnaryFunction<T, A>,
    transform1: UnaryFunction<A, B>,
    transform2: UnaryFunction<B, C>
  ): Observable<C>;
  pipe<A, B, C, D>(
    transform0: UnaryFunction<T, A>,
    transform1: UnaryFunction<A, B>,
    transform2: UnaryFunction<B, C>,
    transform3: UnaryFunction<C, D>
  ): Observable<D>;
  pipe<A, B, C, D, E>(
    transform0: UnaryFunction<T, A>,
    transform1: UnaryFunction<A, B>,
    transform2: UnaryFunction<B, C>,
    transform3: UnaryFunction<C, D>,
    transform4: UnaryFunction<D, E>
  ): Observable<E>;
  pipe<A, B, C, D, E, F>(
    transform0: UnaryFunction<T, A>,
    transform1: UnaryFunction<A, B>,
    transform2: UnaryFunction<B, C>,
    transform3: UnaryFunction<C, D>,
    transform4: UnaryFunction<D, E>,
    transform5: UnaryFunction<E, F>
  ): Observable<F>;
  pipe<A, B, C, D, E, F, G>(
    transform0: UnaryFunction<T, A>,
    transform1: UnaryFunction<A, B>,
    transform2: UnaryFunction<B, C>,
    transform3: UnaryFunction<C, D>,
    transform4: UnaryFunction<D, E>,
    transform5: UnaryFunction<E, F>,
    transform6: UnaryFunction<F, G>
  ): Observable<G>;
  pipe<A, B, C, D, E, F, G>(
    transform0: UnaryFunction<T, A>,
    transform1: UnaryFunction<A, B>,
    transform2: UnaryFunction<B, C>,
    transform3: UnaryFunction<C, D>,
    transform4: UnaryFunction<D, E>,
    transform5: UnaryFunction<E, F>,
    transform6: UnaryFunction<F, G>,
    transform7: UnaryFunction<G, any>,
    ...pipeline: UnaryFunction<any, any>[]
  ): Observable<unknown>;
}

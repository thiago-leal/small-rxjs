import { Unsubscriber } from "./subscription";

export class Observer<T> implements Unsubscriber {
	constructor(
    next: (value: T) => void | Promise<void>,
    error: (value: unknown) => void | Promise<void>,
    complete: () => void | Promise<void>
  );

	unsubscribe(): void;

	setUnsubscribe(unsubscribe: () => void | Promise<void>): void;

	next(value: T): void;
	error(err: unknown): void;
	complete(): void;
}
